def html_link():
    # job_company = job_titles[0].h3.text.strip()
    job_company = ('span', {'class': 'company-name'})
    # < span class ="company-name ellipsis-1" > 小米 < / span >
    # job_titles = ('div', {'class': 'title-info'})
    job_titles = ('span', {'class': 'name ellipsis-1'})
    # <span class="name ellipsis-1">COE负责人</span>
    # job_department = ('div', {'class': 'job-item main-message'})[0].label.text
    job_department = ('dd', {'class': 'ellipsis-1'})
    # <dd class="ellipsis-1">Department: Mobile Phone Department (HW)</dd>
    # job_salary = ('p', {'class': 'job-item-title'})[0].text.strip()
    job_salary = ('span', {'class': 'salary'})
    # <span class="salary">30-50k·14薪</span>
    # job_location = ('p', {'class': 'basic-infor'})[0].text.strip()
    job_properties = ('div', {'class': 'job-properties'})



    # <div class="job-properties">
    #             <!-- 职位地区 -->
    #             <span>Beijing</span>
    #             <!-- 职位工作年限 -->
    #             <span class="split"></span>
    #             <span>5-10 years</span>
    #
    #             <!-- LPT职位实习/应届 -->
    #
    #
    #             <!-- 职位实习/应届 -->
    #             <span class="split"></span>
    #             <span>Bachelor degree or above</span>
    #             <!-- 职位职能名 -->
    #
    #
    #             <!-- 职位人数 -->
    #
    #
    #             <!-- 职位截止日期 -->
    #
    #
    #         </div>
    # job_description = ('div', {'class': 'content content-word'})[0].text.strip()
    job_description = ('dd', {'data-selector': 'job-intro-content'})
    # < dd data-selector = "job-intro-content" > < / dd >
    # job_qualifications = ('div', {'class': 'job-qualifications'})[0].text.strip().split()



    context_dict = {'job_properties':job_properties, 'job_company':job_company, 'job_titles':job_titles, 'job_department':job_department, 'job_salary':job_salary, 'job_description':job_description}

    return context_dict