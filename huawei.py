import os
import requests
import pandas as pd
import time
import set_path
rootPath = set_path.root_path()
datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")
# 8A79481A711B55D84AEBCA5256AA78EC|aa6bd06c3ecde758|1 = os.getenv('8A79481A711B55D84AEBCA5256AA78EC|aa6bd06c3ecde758|1')
# 28083231_912h-vJPFAEKJJPDHPNEKKNJPSLRHEAOIPFBNJ = os.getenv('28083231_912h-vJPFAEKJJPDHPNEKKNJPSLRHEAOIPFBNJ')
# _sn:2$_se:8$_ss:0$_st:1629029878925$dc_visit:2$ses_id:1629026855948%3Bexp-session$_pn:2%3Bexp-session$dc_event:5%3Bexp-session$dc_region:ap-northeast-1%3Bexp-session = os.getenv('_sn:2$_se:8$_ss:0$_st:1629029878925$dc_visit:2$ses_id:1629026855948%3Bexp-session$_pn:2%3Bexp-session$dc_event:5%3Bexp-session$dc_region:ap-northeast-1%3Bexp-session')

cookies = {
    'hwsso_login': '',
    'hwsso_uniportal': '',
    'dtCookie': 'f"3{8A79481A711B55D84AEBCA5256AA78EC|aa6bd06c3ecde758|1}"',
    'rxVisitor': '16290021057477D6JI4S1BHJ40UL578MUDMRV082H7EKQ',
    'rxvt': '1629029892101|1629028082661',
    'dtLatC': '2',
    'dtPC': 'f"3{28083231_912h-vJPFAEKJJPDHPNEKKNJPSLRHEAOIPFBNJ}"',
    'SZXPROITALENTSOCRECRUITMENT': '0000VNZortntW4vCwa2nw_qNZDZ:szxgvap1352-mwc_CloneID',
    'dtSa': '-',
    'SZXPROITALENTRECCAMPPORTAL': '0000qk3ShTnQxLA0t9iT-CHqw22:szxyap1340-mwc_CloneID',
    'utag_main': 'f"v_id:017b48170e3800229871b3285ce801077003d06f0093c{_sn:2$_se:8$_ss:0$_st:1629029878925$dc_visit:2$ses_id:1629026855948%3Bexp-session$_pn:2%3Bexp-session$dc_event:5%3Bexp-session$dc_region:ap-northeast-1%3Bexp-session}"',
    'channel_category': 'direct',
    'channel_name': 'direct',
    '_ga': 'GA1.2.1011701322.1629002891',
    '_gid': 'GA1.2.479669588.1629002891',
    'currentLanguage': 'zh_CN',
    '_ce.s': 'v11.rlc~1629003011972',
    '_fbp': 'fb.1.1629002891629.147853512',
    'Hm_lpvt_48e5a2ca327922f1ee2bb5ea69bdd0a6': '1629002744',
    'Hm_lvt_48e5a2ca327922f1ee2bb5ea69bdd0a6': '1629002391',
    '_accept_cookie_choose': '1|1|1',
}

headers = {
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Accept-Language': 'en-gb',
    'Host': 'career.huawei.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'https://career.huawei.com/reccampportal/portal5/social-recruitment.html',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'X-Requested-With': 'XMLHttpRequest',
}

params = (
    ('curPage', '1'),
    ('pageSize', '10'),
    ('jobFamilyCode', ''),
    ('deptCode', ''),
    ('keywords', ''),
    ('searchType', '1'),
    ('orderBy', 'P_COUNT_DESC'),
    ('jobType', '1'),
)

response = requests.get('https://career.huawei.com/socRecruitment/services/portal3/portalnew/getJobList/page/10/1', headers=headers, params=params, cookies=cookies)
jobsperpage = response.json()['pageVO']["pageSize"]
totalpages = response.json()['pageVO']["totalPages"]
totaljobs = response.json()['pageVO']["filterStr"]
job_ids = []
count = 0
core_url = 'https://career.huawei.com/socRecruitment/services/portal3/portalnew/getJobList/page/10/'

for i in range(1, int(totalpages)+1):
    page_url = core_url + str(i) + '/'
    params = (
        ('curPage', str(i)),
        ('pageSize', '10'),
        ('jobFamilyCode', ''),
        ('deptCode', ''),
        ('keywords', ''),
        ('searchType', '1'),
        ('orderBy', 'P_COUNT_DESC'),
        ('jobType', '1'),
    )

    response = requests.get(page_url, headers=headers, params=params, cookies=cookies)
    for r in response.json()['result']:
        count += 1
        print(count, r['jobId'], r['jobname'])
        job_ids.append(r['jobId'])

jobs = []
count = 0
for job_id in job_ids:
    job_dict = {}
    job_company = ''
    job_title = ''
    job_department = ''
    working_years = ''
    job_salary = ''
    education = ''
    job_location = ''
    job_description = ''

    params = (
        ('jobId', str(job_id)),
    )

    response = requests.get('https://career.huawei.com/socRecruitment/services/portal/portalpub/getJobDetail', headers=headers, params=params, cookies=cookies)

    r = response.json()
    count += 1
    try:
        job_published = r["creationDate"]
    except KeyError:
        job_published = ''
    job_company = '华为'
    job_title = r['jobname']
    try:
        deptName = r['deptName']
    except KeyError:
        deptName = ''
    try:
        jobFamilyName = r['jobFamilyName']
    except KeyError:
        jobFamilyName = ''
    job_department = 'deptName: ' + deptName + ',  jobFamilyName: ' + jobFamilyName
    working_years = ''
    job_salary = ''
    job_education = ''
    job_location = r['jobArea']
    job_requirements = r['jobRequire']
    job_description = r['mainBusiness']
    print(count, job_company, job_department, job_title, job_salary, job_location)
    job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department, ' 工作年限': working_years,
                '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD (requirements)': job_requirements,
                'JD（经验，工作内容）': job_description,
                'Published': job_published}
    jobs.append(job_dict)

data = pd.DataFrame(jobs)
data.to_excel(rootPath + datestr + '_' + timestr + '_' + job_company +'.xlsx', engine='xlsxwriter')
