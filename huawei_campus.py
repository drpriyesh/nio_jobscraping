import os
import requests
import pandas as pd
import time
import set_path
rootPath = set_path.root_path()
datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")
# 8A79481A711B55D84AEBCA5256AA78EC|aa6bd06c3ecde758|1 = os.getenv('8A79481A711B55D84AEBCA5256AA78EC|aa6bd06c3ecde758|1')
# 28083231_912h-vJPFAEKJJPDHPNEKKNJPSLRHEAOIPFBNJ = os.getenv('28083231_912h-vJPFAEKJJPDHPNEKKNJPSLRHEAOIPFBNJ')
# _sn:2$_se:8$_ss:0$_st:1629029878925$dc_visit:2$ses_id:1629026855948%3Bexp-session$_pn:2%3Bexp-session$dc_event:5%3Bexp-session$dc_region:ap-northeast-1%3Bexp-session = os.getenv('_sn:2$_se:8$_ss:0$_st:1629029878925$dc_visit:2$ses_id:1629026855948%3Bexp-session$_pn:2%3Bexp-session$dc_event:5%3Bexp-session$dc_region:ap-northeast-1%3Bexp-session')
cookies = {
    'hwsso_login': '',
    'hwsso_uniportal': '',
    'dtCookie': 'f"3{8A79481A711B55D84AEBCA5256AA78EC|aa6bd06c3ecde758|1}"',
    'dtPC': 'f"3{28083231_912h-vJPFAELEKPEBMHEKKNJOPIRACDEIPFBNI}"',
    'dtSa': 'true%7CC%7C-1%7CCampus%20recruitment%7C-%7C1629036175553%7C28083231_912%7Chttps%3A%2F%2Fcareer.huawei.com%2Freccampportal%2Fportal5%2Findex.html%7CHuawei%20Recruitment-Job%20Search-Job%20Search-Latest%20Recruitment%20Information-Huawei%20Recruitment%20Official%20%7C1629028092104%7C',
    'rxvt': '1629037975641|1629035385064',
    'SZXPROITALENTRECCAMPPORTAL': '0000TMtQh-Hg-lnD7DW5zseSiEI:szxyap1340-mwc_CloneID',
    'SZXPROITALENTSOCRECRUITMENT': '0000Zdc2ZjJqNqVCU03K8CyZywm:szxgvap1352-mwc_CloneID',
    'rxVisitor': '16290021057477D6JI4S1BHJ40UL578MUDMRV082H7EKQ',
    'dtLatC': '2',
    'utag_main': 'f"v_id:017b48170e3800229871b3285ce801077003d06f0093c{_sn:2$_se:8$_ss:0$_st:1629029878925$dc_visit:2$ses_id:1629026855948%3Bexp-session$_pn:2%3Bexp-session$dc_event:5%3Bexp-session$dc_region:ap-northeast-1%3Bexp-session}"',
    'channel_category': 'direct',
    'channel_name': 'direct',
    '_ga': 'GA1.2.1011701322.1629002891',
    '_gid': 'GA1.2.479669588.1629002891',
    'currentLanguage': 'zh_CN',
    '_ce.s': 'v11.rlc~1629003011972',
    '_fbp': 'fb.1.1629002891629.147853512',
    'Hm_lpvt_48e5a2ca327922f1ee2bb5ea69bdd0a6': '1629002744',
    'Hm_lvt_48e5a2ca327922f1ee2bb5ea69bdd0a6': '1629002391',
    '_accept_cookie_choose': '1|1|1',
}

headers = {
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Accept-Language': 'en-gb',
    'Host': 'career.huawei.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'https://career.huawei.com/reccampportal/portal5/campus-recruitment.html?jobTypes=0',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'X-Requested-With': 'XMLHttpRequest',
}

campus_codes = {'博士生':{'jobType':'2', 'jobTypes':''},
'本硕':{'jobTypes': '1', 'jobType': '0'},
'实习生':{'jobTypes': '0', 'jobType': '0'},
'留学生':{'jobTypes': '1', 'jobType': '0'},
'海外本地':{'jobTypes': '7', 'jobType': '0'}}

for k, v in campus_codes.items():
    print(k, v)
    jtypes = v['jobTypes']
    jtype = v['jobType']
    params = (
        ('curPage', '1'),
        ('pageSize', '10'),
        ('jobTypes', str(jtypes)),
        ('jobType',str(jtype)),
        ('jobFamClsCode', ''),
        ('searchText', ''),
        ('deptCode', ''),
        ('graduateItem', ''),
        ('reqTime', '1629036284911'),
        ('orderBy', 'ISS_STARTDATE_DESC_AND_IS_HOT_JOB'),
    )

    response = requests.get('https://career.huawei.com/reccampportal/services/portal/portalpub/getJob/page/10/1', headers=headers, params=params, cookies=cookies)

    jobsperpage = response.json()['pageVO']["pageSize"]
    totalpages = response.json()['pageVO']["totalPages"]
    totaljobs = response.json()['pageVO']["filterStr"]
    job_ids = []
    count = 0
    core_url = 'https://career.huawei.com/reccampportal/services/portal/portalpub/getJob/page/10/'

    for i in range(1, int(totalpages)+1):
        page_url = core_url + str(i) + '/'
        params = (
            ('curPage', str(i)),
            ('pageSize', '10'),
            ('jobTypes', str(jtypes)),
            ('jobType', str(jtype)),
            ('jobFamClsCode', ''),
            ('searchText', ''),
            ('deptCode', ''),
            ('graduateItem', ''),
            ('reqTime', '1629036284911'),
            ('orderBy', 'ISS_STARTDATE_DESC_AND_IS_HOT_JOB'),
        )

        response = requests.get(page_url, headers=headers, params=params, cookies=cookies)
        for r in response.json()['result']:
            count += 1
            print(count, r['jobId'], r['jobname'])
            job_ids.append(r['jobId'])

    jobs = []
    count = 0
    for job_id in job_ids:
        job_dict = {}
        job_company = ''
        job_title = ''
        job_department = ''
        working_years = ''
        job_salary = ''
        education = ''
        job_location = ''
        job_description = ''

        params = (
            ('jobId', str(job_id)),
        )

        response = requests.get('https://career.huawei.com/reccampportal/services/portal/portalpub/getJobDetail',
                                headers=headers, params=params, cookies=cookies)
        r = response.json()
        count += 1
        try:
            job_published = r["creationDate"]
        except KeyError:
            job_published = ''
        job_company = '华为'
        job_title = r['jobname']
        try:
            deptName = r['deptName']
        except KeyError:
            deptName = ''
        try:
            jobFamilyName = r['jobFamilyName']
        except KeyError:
            jobFamilyName = ''
        job_department = 'deptName: ' + deptName + ',  jobFamilyName: ' + jobFamilyName
        working_years = ''
        job_salary = ''
        job_education = ''
        job_location = r['jobArea']
        job_requirements = r['jobRequire']
        job_description = r['mainBusiness']
        print(count, job_company, job_department, job_title, job_salary, job_location)
        job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department, ' 工作年限': working_years,
                    '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD (requirements)': job_requirements,
                    'JD（经验，工作内容）': job_description,
                    'Published': job_published}
        jobs.append(job_dict)

    data = pd.DataFrame(jobs)
    data.to_excel(rootPath + datestr + '_' + timestr + '_'  + job_company + '_' + k +'.xlsx', engine='xlsxwriter')
