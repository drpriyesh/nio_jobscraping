from collections import Counter
from jieba_fast import cut as j
from jieba_fast import posseg as pseg

with open(TEXT_DIR + filename + '.txt', encoding="utf8", errors='ignore') as f:
    inputSample = f.read()
inputSample_list = j(inputSample, cut_all=False)
inputSampleCnt = dict(Counter(inputSample_list))
words = pseg.cut(inputSample)