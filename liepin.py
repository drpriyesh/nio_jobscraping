import requests
import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
import pandas as pd
import time

cookies1 = {
    'Hm_lpvt_a2647413544f5a04f00da7eee0d5e200': '1629688588',
    'Hm_lvt_a2647413544f5a04f00da7eee0d5e200': '1629078413,1629096724,1629259950,1629688175',
    '__session_seq': '5',
    '__uv_seq': '5',
    'JSESSIONID': '76D3673CE8915BF5EC2F78DB80545831',
    'fe_se': '-1629688175538',
    '__tlog': '1629688167139.09%7C00000000%7C00000000%7C00000000%7C00000000',
    '__uuid': '1629688167135.39',
    '_fecdn_': '1',
    'acw_tc': '276082a916296881587066773e1f3e185fbaad90af97648cbe47386d0196bb',
    'imClientId': 'a9595563875a0c1a899a2b3166202978',
    'imClientId_0': 'a9595563875a0c1a899a2b3166202978',
    'imId': 'a9595563875a0c1adea73efffbfa56b7',
    'imId_0': 'a9595563875a0c1adea73efffbfa56b7',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_last_sent_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'gr_user_id': '3c68664b-a27f-44af-a0f8-ab7c388a724f',
    'c_flag': '4731987fdb518092b289662abb2d96f1',
    'need_bind_tel': 'false',
    'new_user': 'false',
    'lt_auth': '7bkCM3IGnVWt5XCKiTdc46obi4mtWT%2FO9H9Y0E8Dg9G8CfHg4P%2FiRwKAqbYD%2BCoIq0kmdP4zMLb8%0D%0APOj2y3ND60MW8FGnl5%2Buv%2Fi6z30FTvpnIMW2vezHg%2FXSQp4ilEAC8nJbpEIL%2BQ%3D%3D%0D%0A',
    '__s_bid': '9e802e9a13cabf2fd869d971e28f8f1b67f4',
}

headers1 = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Host': 'www.liepin.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Accept-Language': 'en-gb',
    'Referer': 'https://www.liepin.com/zhaopin/',
    'Connection': 'keep-alive',
}


cookies2 = {
    'Hm_lpvt_a2647413544f5a04f00da7eee0d5e200': '1629689412',
    'Hm_lvt_a2647413544f5a04f00da7eee0d5e200': '1629078413,1629096724,1629259950,1629688175',
    '__session_seq': '10',
    '__tlog': '1629688167139.09%7C00000000%7CR000000075%7C00000000%7C00000000',
    '__uv_seq': '10',
    'JSESSIONID': '54FFDEECFDE73842DFBE85A82E9477EC',
    'fe_se': '-1629688175538',
    '__uuid': '1629688167135.39',
    '_fecdn_': '1',
    'acw_tc': '276082a916296881587066773e1f3e185fbaad90af97648cbe47386d0196bb',
    'imClientId': 'a9595563875a0c1a899a2b3166202978',
    'imClientId_0': 'a9595563875a0c1a899a2b3166202978',
    'imId': 'a9595563875a0c1adea73efffbfa56b7',
    'imId_0': 'a9595563875a0c1adea73efffbfa56b7',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_last_sent_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'gr_user_id': '3c68664b-a27f-44af-a0f8-ab7c388a724f',
    'c_flag': '4731987fdb518092b289662abb2d96f1',
    'need_bind_tel': 'false',
    'new_user': 'false',
    'lt_auth': '7bkCM3IGnVWt5XCKiTdc46obi4mtWT%2FO9H9Y0E8Dg9G8CfHg4P%2FiRwKAqbYD%2BCoIq0kmdP4zMLb8%0D%0APOj2y3ND60MW8FGnl5%2Buv%2Fi6z30FTvpnIMW2vezHg%2FXSQp4ilEAC8nJbpEIL%2BQ%3D%3D%0D%0A',
    '__s_bid': '9e802e9a13cabf2fd869d971e28f8f1b67f4',
}

headers2 = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Host': 'www.liepin.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Accept-Language': 'en-gb',
    'Referer': 'https://www.liepin.com/zhaopin/?compkind=&dqs=&pubTime=&pageSize=40&salary=&compTag=&sortFlag=15&degradeFlag=0&compIds=&subIndustry=&jobKind=&industries=&compscale=&key=%E5%B0%8F%E7%B1%B3&siTag=K-RGV3diu2zshoH14tZ9GA%7EfA9rXquZc5IkJpXC-Ycixw&d_sfrom=search_prime&d_ckId=6cd6dce427572c0a9113452fecc38e3b&d_curPage=2&d_pageSize=40&d_headId=6cd6dce427572c0a9113452fecc38e3b&curPage=0',
    'Connection': 'keep-alive',
}




datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")
company_key = { 'xiaomi': {'key':"\u5C0F\u7C73", 'no_jobs':5003},
                'jidu': {'key':"\u96C6\u5EA6",'no_jobs':437},
                'huawei': {'key':'\u534E\u4E3A', 'no_jobs':5336},
                'lixiang': {'key':'\u7406\u60F3\u6C7D\u8F66', 'no_jobs':1261},
                'sensetime': {'key':'\u5546\u6C64\u79D1\u6280SenseTime', 'no_jobs':1593},
                'ponyai': {'key':'\u5C0F\u9A6C\u667A\u884C', 'no_jobs':320},
             }
c= (input('Enter company name (xiaomi, jidu, huawei, lixiang, sensetime, ponyai) or leave blank and press enter to run for all: '))
# c = ['jidu','ponyai'] #'xiaomi', 'huawei', 'lixiang', 'sensetime',
print(c)
# c = 'xiaomi'
def lieping(c):

    page_size = 40
    key = company_key[c]['key']
    pages = int(company_key[c]['no_jobs']/page_size)+1
    print(key, pages)
    count=0
    job_urls = []
    time_index = []

    for page in range(0,pages):
        if page is 0:
            dpage=0
        else:
            dpage=page-1
        params = (
            ('industries', ''),
            ('subIndustry', ''),
            ('dqs', ''),
            ('salary', ''),
            ('jobKind', ''),
            ('pubTime', ''),
            ('compkind', ''),
            ('compscale', ''),
            ('searchType', '1'),
            ('isAnalysis', ''),
            ('sortFlag', '15'),
            ('d_headId', '6cd6dce427572c0a9113452fecc38e3b'),
            ('d_ckId', '6cd6dce427572c0a9113452fecc38e3b'),
            ('d_curPage', str(dpage)),
            ('d_pageSize', '40'),
            ('siTag', 'K-RGV3diu2zshoH14tZ9GA~fA9rXquZc5IkJpXC-Ycixw'),
            ('d_sfrom', 'search_prime'),
            ('compIds', ''),
            ('compTag', ''),
            ('key', key),
            ('curPage', str(page)),
            ('pageSize', '40')
        )

        response = requests.get('https://www.liepin.com/zhaopin/', headers=headers1, params=params, cookies=cookies1)

        soup = BeautifulSoup(response.text, 'lxml')

        # job_info = soup.find_all('div', {'class': 'job-info'}, 'h3')
        job_info = soup.find_all('h3')

        times = soup.find_all('time')
        # print(job_info)

        for time in times:
            tTags = time['title']
            time_index.append(tTags)
            # print(tTags)

        for tag in job_info:
            aTags = tag.find_all("a", {"href": True})
            for tag in aTags:
                count+=1
                print(count, tag.text.strip().replace(" ",""), tag["href"])
                job_urls.append(tag["href"])

    jobs = []
    count = 0
    for job_url in job_urls:
        time = time_index[count]
        if 'https://www.liepin.com/job' not in job_url:
            job_url = 'https://www.liepin.com' + job_url
        else:
            pass

        count += 1
        job_dict = {}

        job_company = ''
        job_title = ''
        job_department = ''
        working_years = ''
        job_salary = ''
        job_education = ''
        job_location = ''
        job_description = ''

        params = (
            ('imscid', 'R000000075'),
            ('siTag', 'K-RGV3diu2zshoH14tZ9GA~fA9rXquZc5IkJpXC-Ycixw'),
            ('d_sfrom', 'search_prime'),
            ('d_ckId', '6cd6dce427572c0a9113452fecc38e3b'),
            ('d_curPage', '0'),
            ('d_pageSize', '40'),
            ('d_headId', '6cd6dce427572c0a9113452fecc38e3b'),
            ('d_posi', '0'),
        )

        response = requests.get(job_url, headers=headers2,  params=params, cookies=cookies2)
        soup = BeautifulSoup(response.text, 'lxml')
        job_titles = soup.find_all('div', {'class': 'title-info'})

        try:
            job_department = soup.find_all('div', {'class': 'job-item main-message'})[0].label.text
        except IndexError:
            print("IndexErr job_department")
            job_department = ''
        try:
            job_title = job_titles[0].h1.text.strip()
        except IndexError:
            print("IndexErr job_title")
            job_title = ''
        try:
            job_company = job_titles[0].h3.text.strip()
        except IndexError:
            print("IndexErr job_company")
            job_company = ''
        try:
            job_salary = soup.find_all('p', {'class': 'job-item-title'})[0].text.strip()
        except IndexError:
            print("IndexErr job_salary")
            job_salary = ''
        try:
            job_location = soup.find_all('p', {'class': 'basic-infor'})[0].text.strip()
        except IndexError:
            print("IndexErr job_location")
            job_location = ''
        try:
            job_description = soup.find_all('div', {'class': 'content content-word'})[0].text.strip()
        except IndexError:
            print("IndexErr job_description")
            job_description = ''
        try:
            job_qualifications = soup.find_all('div', {'class': 'job-qualifications'})[0].text.strip().split() #Bachelor degree or above, 3-5 years, language unlimited, age limit
            [job_education, working_years, language_req, age_req] = job_qualifications
        except IndexError:
            print("IndexErr job_qualifications")
            job_qualifications = ''
            [job_education, working_years, language_req, age_req] = ['','','','']
        except ValueError:
            try:
                [job_education, working_years, language_req1, plus, language_req2, age_req] = job_qualifications
                language_req = language_req1 +', '+ language_req2
                print("ValErr language_req")
            except ValueError:
                [job_education, working_years, language_req1, plus, language_req2, plus2, language_req3, age_req] = job_qualifications
                language_req = language_req1 + ', ' + language_req2 + ', ' + language_req3
                print("ValErr language_req")
        # print(count, job_title, job_company, job_salary, job_location, job_qualifications, job_description)
        print(count, job_company, job_department, job_title, job_salary, job_location, time)
        job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department,' 工作年限': working_years, '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD（经验，工作内容）': job_description, 'Published': time, 'Link':job_url}
        jobs.append(job_dict)
        # print(job_dict)

    data=pd.DataFrame(jobs)
    data.to_excel('/Users/priyeshpatel/PycharmProjects/Nio_jobscrape/' + datestr + '_' + timestr + '_Liepin_' + job_company + '.xlsx')


if type(c) is list:
    for comp in c:
        lieping(comp)
else:
    lieping(c)
