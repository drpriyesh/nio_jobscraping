import requests
import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
import pandas as pd
import time
import set_path
import html_link
rootPath = set_path.root_path()
cookies1 = {
    'Hm_lpvt_a2647413544f5a04f00da7eee0d5e200': '1629693405',
    'Hm_lvt_a2647413544f5a04f00da7eee0d5e200': '1629078413,1629096724,1629259950,1629688175',
    '__session_seq': '150',
    '__uv_seq': '150',
    'JSESSIONID': '582986E522FA0B446CC1728AB606E478',
    'acw_tc': '3ccdc14d16296918715813266e5fb6d876097225b4b2858b9532e3f8320cf5',
    '__tlog': '1629688167139.09%7C00000000%7CR000000075%7C00000000%7C00000000',
    'fe_se': '-1629688175538',
    '__uuid': '1629688167135.39',
    '_fecdn_': '1',
    'imClientId': 'a9595563875a0c1a899a2b3166202978',
    'imClientId_0': 'a9595563875a0c1a899a2b3166202978',
    'imId': 'a9595563875a0c1adea73efffbfa56b7',
    'imId_0': 'a9595563875a0c1adea73efffbfa56b7',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_last_sent_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'gr_user_id': '3c68664b-a27f-44af-a0f8-ab7c388a724f',
    'c_flag': '4731987fdb518092b289662abb2d96f1',
    'need_bind_tel': 'false',
    'new_user': 'false',
    '__s_bid': '9e802e9a13cabf2fd869d971e28f8f1b67f4',
}

headers1 = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Host': 'www.liepin.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Accept-Language': 'en-gb',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
}


cookies2 = {
    'Hm_lpvt_a2647413544f5a04f00da7eee0d5e200': '1629701320',
    'Hm_lvt_a2647413544f5a04f00da7eee0d5e200': '1629078413,1629096724,1629259950,1629688175',
    '__session_seq': '164',
    '__uv_seq': '164',
    'JSESSIONID': '7C184D49A98DDBC3E5DEB2AD6646073E',
    'acw_tc': '2760828f16297013170393815e251647b0aa148f7dc2a45170f0a3dc07e337',
    '__tlog': '1629688167139.09%7C00000000%7CR000000075%7C00000000%7C00000000',
    'fe_se': '-1629688175538',
    '__uuid': '1629688167135.39',
    '_fecdn_': '1',
    'imClientId': 'a9595563875a0c1a899a2b3166202978',
    'imClientId_0': 'a9595563875a0c1a899a2b3166202978',
    'imId': 'a9595563875a0c1adea73efffbfa56b7',
    'imId_0': 'a9595563875a0c1adea73efffbfa56b7',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'bad1b2d9162fab1f80dde1897f7a2972_gr_last_sent_cs1': '0fbe729da0e61bc26bc56763ae71c6f2',
    'gr_user_id': '3c68664b-a27f-44af-a0f8-ab7c388a724f',
    'c_flag': '4731987fdb518092b289662abb2d96f1',
    'need_bind_tel': 'false',
    'new_user': 'false',
    '__s_bid': '9e802e9a13cabf2fd869d971e28f8f1b67f4',
}

headers2 = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Host': 'www.liepin.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Accept-Language': 'en-gb',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
}




datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")
company_key = { 'xiaomi': {'key':"https://www.liepin.com/company-jobs/2174886/", 'no_jobs':5083},
                'jidu': {'key':"https://www.liepin.com/company-jobs/12737907/",'no_jobs':452},
                'huawei': {'key':'https://www.liepin.com/company-jobs/954482/', 'no_jobs':5436},
                'lixiang': {'key':'https://www.liepin.com/company-jobs/8572825/', 'no_jobs':1265},
                'sensetime': {'key':'https://www.liepin.com/company-jobs/8349238/', 'no_jobs':1103},
                'ponyai1': {'key':'https://www.liepin.com/company-jobs/10187083/', 'no_jobs':144},
                'ponyai2': {'key':'https://www.liepin.com/company-jobs/9382502/', 'no_jobs':176},
             }
# c= (input('Enter company name (xiaomi, jidu, huawei, lixiang, sensetime, ponyai) or leave blank and press enter to run for all: '))
c = ['xiaomi','huawei','lixiang','sensetime','jidu','ponyai1','ponyai2'] #
print(c)
# c = 'xiaomi'
def lieping(c):

    page_size = 30
    key = company_key[c]['key']
    pages = int(company_key[c]['no_jobs']/page_size)+1
    if pages > 100:
        pages = 101
    print(key, pages)
    count=0
    job_urls = []
    time_index = []

    for page in range(1, 3):

        page_str = key + 'pn' + str(page) + '/'

        response = requests.get(page_str, headers=headers1, cookies=cookies1)

        soup = BeautifulSoup(response.text, 'lxml')

        job_info = soup.find_all('a', {'class': 'title'})
        print('job_info: ', job_info)
        # job_info = soup.find_all('h3')

        times = soup.find_all('time')
        # print(job_info)

        for time in times:
            tTags = time['title']
            time_index.append(tTags)
            # print(tTags)

        for tag in job_info:
            count += 1
            print(count, tag.text.strip().replace(" ",""), 'https://www.liepin.com/' + tag['href'][22:len(tag['href'])])

            job_urls.append('https://www.liepin.com/' + tag['href'][22:len(tag['href'])])
        # for tag in job_info:
        #     aTags = tag.find_all("a", {"href": True})
        #     for tag in aTags:
        #         count+=1
        #         print(count, tag.text.strip().replace(" ",""), tag["href"])
        #         job_urls.append(tag["href"])

    jobs = []
    count = 0
    for job_url in job_urls:
        time = time_index[count]
        # if 'https://www.liepin.com/job' not in job_url:
        #     job_url = 'https://www.liepin.com' + job_url
        # else:
        #     pass

        count += 1
        job_dict = {}

        job_company = ''
        job_title = ''
        job_department = ''
        working_years = ''
        job_salary = ''
        job_education = ''
        job_location = ''
        job_description = ''

        htmlLink = html_link.html_link()


        response = requests.get(job_url, headers=headers2, cookies=cookies2)
        print('response.text:',response.text)
        soup = BeautifulSoup(response.text, 'lxml')
        job_titles = soup.find_all('div', {'class': 'title-info'})
        print('job_titles:', job_titles)

        try:
            job_department = soup.find_all('div', {'class': 'job-item main-message'})[0].label.text
        except IndexError:
            print("IndexErr job_department")
            job_department = ''
        try:
            job_title = job_titles[0].h1.text.strip()
        except IndexError:
            print("IndexErr job_title")
            job_title = ''
        try:
            job_company = job_titles[0].h3.text.strip()
        except IndexError:
            print("IndexErr job_company")
            job_company = ''
        try:
            job_salary = soup.find_all('p', {'class': 'job-item-title'})[0].text.strip()
        except IndexError:
            print("IndexErr job_salary")
            job_salary = ''
        try:
            job_location = soup.find_all('p', {'class': 'basic-infor'})[0].text.strip()
        except IndexError:
            print("IndexErr job_location")
            job_location = ''
        try:
            job_description = soup.find_all('div', {'class': 'content content-word'})[0].text.strip()
        except IndexError:
            print("IndexErr job_description")
            job_description = ''

        try:

            job_qualifications = soup.find_all('div', {'class': 'job-qualifications'})[0].text.strip().split() #Bachelor degree or above, 3-5 years, language unlimited, age limit
            print(job_url,  job_qualifications)
            [job_education, working_years, language_req, age_req] = job_qualifications
        except IndexError:
            print("IndexErr job_qualifications")
            job_qualifications = ''
            [job_education, working_years, language_req, age_req] = ['','','','']
        except ValueError:
            try:
                [job_education, working_years, language_req1, plus, language_req2, age_req] = job_qualifications
                language_req = language_req1 +', '+ language_req2
                print("ValErr language_req")
            except ValueError:
                try:
                    [job_education, working_years, language_req1, plus, language_req2, plus2, language_req3, age_req] = job_qualifications
                    language_req = language_req1 + ', ' + language_req2 + ', ' + language_req3
                    print("ValErr language_req")
                except ValueError:
                    try:
                        [job_education, working_years] = job_qualifications
                    except ValueError:
                        job_education = job_qualifications
                        working_years = job_qualifications
        # print(count, job_title, job_company, job_salary, job_location, job_qualifications, job_description)
        print(count, job_company, job_department, job_title, job_salary, job_location, time)
        job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department,' 工作年限': working_years, '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD（经验，工作内容）': job_description, 'Published': time, 'Link':job_url}
        jobs.append(job_dict)
        # print(job_dict)

    data=pd.DataFrame(jobs)
    data.to_excel(rootPath + datestr + '_' + timestr + '_Liepin_method2' + job_company + '.xlsx')


if type(c) is list:
    for comp in c:
        lieping(comp)
else:
    lieping(c)
