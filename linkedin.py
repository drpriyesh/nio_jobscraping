from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
# direct the webdriver to where the browser file is:
driver_path = "/Users/priyeshpatel/PycharmProjects/Nio_jobscrape/chromedriver 2"

driver = webdriver.Chrome(executable_path=driver_path)
# your secret credentials:
email = "patelpriyesh@hotmail.com"
password = "Aa01081954"
# Go to linkedin and login
driver.get('https://www.linkedin.com/login')
time.sleep(3)
driver.find_element_by_id('username').send_keys(email)
driver.find_element_by_id('password').send_keys(password)
driver.find_element_by_id('password').send_keys(Keys.RETURN)

driver.get("https://www.linkedin.com/jobs/")
time.sleep(3)
# find the keywords/location search bars:
search_bars = driver.find_elements_by_class_name('jobs-search-box__text-input')
search_keywords = search_bars[0]
keywords = '小米'
search_keywords.send_keys(keywords)

search_location = search_bars[2]
location = 'Shanghai'
search_location.send_keys(location)
search_location.send_keys(Keys.RETURN)

list_items = driver.find_elements_by_class_name("occludable-update")
# scrolls a single page:
for job in list_items:
    # executes JavaScript to scroll the div into view
    driver.execute_script("arguments[0].scrollIntoView();", job)
    job.click()
    time.sleep(3)
    # get info:
    [position, company, location] = job.text.split('\n')[:3]
    details = driver.find_element_by_id("job-details").text
