import requests
import pandas as pd
import time
from bs4 import BeautifulSoup
import set_path
rootPath = set_path.root_path()
datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")
cookies = {
    'share-uid': '',
    'a3c49546c2476d21_gr_session_id': 'e9f4d75c-a4c7-4365-a335-883498b8b935',
    'a3c49546c2476d21_gr_session_id_e9f4d75c-a4c7-4365-a335-883498b8b935': 'true',
    'gr_user_id': '367614a2-49ac-4dee-8e1c-6cf2cc48f18e',
    'sensorsdata2015jssdkcross': '%7B%22distinct_id%22%3A%2217b439101789e0-086f3359df19ce-49193101-1296000-17b43910179932%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_landing_page%22%3A%22https%3A%2F%2Fwww.lixiang.com%2F%22%7D%2C%22%24device_id%22%3A%2217b439101789e0-086f3359df19ce-49193101-1296000-17b43910179932%22%7D',
    'sid': 'f047c7cc-2012-46e2-bd18-02cf3de1f28c',
    'X-CHJ-SourceUrl': 'https%3A%2F%2Fwww.lixiang.com%2F%3Fchjchannelcode%3D102002',
    'uid': 'ce7fb391-2b9b-4116-a2ea-4bbe70e75e4e',
    'X-LX-Deviceid': '1c7ca6df-0ff7-597c-457c-f4c9d278ef76',
}

headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Host': 'www.lixiang.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Accept-Language': 'en-gb',
    'Referer': 'https://www.lixiang.com/job.html',
    'Connection': 'keep-alive',
}

params = (
    ('pageNo', '1'),
    ('pageSize', '10'),
)

response = requests.get('https://api-web.lixiang.com/cms-api/v1-0/recruit/job-page', headers=headers, params=params, cookies=cookies)
# print(response.json()['data'])


jobsperpage = response.json()['data']["pageSize"]
totalpages = response.json()['data']["pageCount"]
totaljobs = response.json()['data']["totalCount"]
job_ids = []
count = 0
for i in range(1, int(totalpages)+1):
    params = (
        ('pageNo', str(i)),
        ('pageSize', '10'),
    )
    response = requests.get('https://api-web.lixiang.com/cms-api/v1-0/recruit/job-page', headers=headers, params=params,
                            cookies=cookies)

    for r in response.json()['data']['results']:
        count += 1
        print(count, r['id'], r['title'])
        job_ids.append(r['id'])


params = (
    ('limit', '1000'),
)

core_url = 'https://api-web.lixiang.com/cms-api/v1-0/recruit/job/'
jobs = []
count = 0

for job_id in job_ids:
    job_dict = {}
    job_company = ''
    job_title = ''
    job_department = ''
    working_years = ''
    job_salary = ''
    education = ''
    job_location = ''
    job_description = ''
    job_url = core_url + str(job_id)
    response = requests.get(job_url, headers=headers, cookies=cookies)
    # print(response.json()['data'])

    r = response.json()['data']
    count+=1

    job_company = '理想'
    job_title = r['title']
    job_department = 'department: ' + r['departmentName'] + ', zhineng: ' + r['zhinengName']
    working_years = str(r['minExperience'])+'-'+str(r['maxExperience'])
    job_salary = str(r['minSalary'])+'-'+str(r['maxSalary'])
    education = r['education']
    try:
        job_location = r['locationCity']+' '+r['locationAddress']
    except TypeError:
        if not r['locationAddress']:
            job_location = r['locationCity']
        if not ['locationCity'] and not r['locationAddress']:
            job_location = ''
    if not r['description']:
        job_description = ''
    else:
        job_description = BeautifulSoup(r['description']).text
    # job_company = ''
    # job_title = r['']
    # job_department = r['']
    # working_years = r['']
    # job_salary = r['']
    # education = r['']
    # job_location = r['']
    # job_description = r['']

    try:
        commit = 'commitment: ' + r['commitment']
    except KeyError:
        commit = 'commitment: blank'
    try:
        hire = 'hireMode: ' + r['hireMode']
    except KeyError:
        hire = 'hireMode: blank'
    other = commit + ', ' + hire
    try:
        job_published = r["openedTime"]
    except KeyError:
        job_published = ''

    print(count, job_company, job_department, job_title, job_salary, job_location)
    job_salary = ''
    job_education = ''
    working_years = ''
    job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department, ' 工作年限': working_years,
                '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD（经验，工作内容）': job_description,
                'Published': job_published}
    jobs.append(job_dict)
data=pd.DataFrame(jobs)
data.to_excel(rootPath + datestr + '_' + timestr + '_' + job_company +'.xlsx', engine='xlsxwriter')
