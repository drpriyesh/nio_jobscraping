import requests
import pandas as pd
import os
from pickle_dat import pickling, unpickling
import xlsxwriter
import time
from bs4 import BeautifulSoup
import set_path
rootPath = set_path.root_path()
datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")

cookies = {
    'connect.sid': 's%3AILYGonTpvjlhv49LmgF3QBb0CqEy5Dn4.supUvSifjujOJ5eNmpmhDoXPThChu1mo%2FAO4orPz8Gc',
    'locale': 'zh-CN',
    'acw_tc': '2760826116290074194764281e5263e470256163bf2d5c9ea42ee5ac733acb',
    'moka-apply': 'HLZ0g7oEc%2FNb4TxZ5bA72%2BfJqKewALyyjerW8GhTBEw%3D',
}

headers = {
    'Content-Type': 'application/json',
    'Accept': '*/*',
    'Accept-Language': 'en-gb',
    'Accept-Encoding': 'gzip, deflate, br',
    'Host': 'app.mokahr.com',
    'Origin': 'https://app.mokahr.com',
    'Content-Length': '84',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'https://app.mokahr.com/apply/pony/2736',
    'Connection': 'keep-alive',
    'use-http-status': '0',
    'moka-tracing': '{"op_no":"356f0615-6577-4a95-bbd3-590f1b87f248","locale":"zh_CN","time_zone":"GMT+08:00","source":"apply-web","org_id":"pony"}',
}

data1 = '{"limit":15,"offset":0,"siteId":2736,"orgId":"pony","site":"social","needStat":true}'
data2 = '{"limit":15,"offset":0,"zhinengId":"15065","siteId":2735,"orgId":"pony","site":"campus","needStat":true}'
data3 = '{"limit":15,"offset":0,"zhinengId":"48772","siteId":2735,"orgId":"pony","site":"campus","needStat":true}'
data1_ext1 = ',"siteId":2736,"orgId":"pony","site":"social","needStat":true}'
data2_ext1 = ', "zhinengId":"15065", "siteId":2735, "orgId":"pony", "site":"campus", "needStat":true}'
data3_ext1 = ', "zhinengId":"48772", "siteId":2735, "orgId":"pony", "site":"campus", "needStat":true}'
data1_ext2 =  '", "siteId":2736}'
data2_ext2 =  '", "siteId":2735}'
data3_ext2 =  '", "siteId":2735}'

cities = {'440115':'广州，南沙','110108':'北京，海胆','310114':'上海，嘉定','110112':'北京，通州','440305':'深圳，南山'}


ponyList = [('',data1, data1_ext1, data1_ext2),('_campus',data2, data2_ext1, data2_ext2),('_intern',data3, data3_ext1, data3_ext2)]
for name, a, b, c in ponyList:

    print(name, a, b, c)
    data = a
    limit=15
    response = requests.post('https://app.mokahr.com/api/outer/ats-jc-apply/website/jobs', headers=headers, cookies=cookies, data=data)

    no_jobs = int(response.json()['data']['jobStats']['total'])
    no_pages = int(no_jobs/limit)+1
    r=[]
    count = 0
    for i in range (0,no_jobs,limit):
        # print(i)
        data = '{"limit":' + str(limit) + ',"offset":' + str(i) + b
        response = requests.post('https://app.mokahr.com/api/outer/ats-jc-apply/website/jobs', headers=headers, cookies=cookies, data=data)
        for line in response.json()['data']['jobs']:
            count += 1
            print(count, line['title'])

        r.append(response.json()['data']['jobs'])
        no_jobs = response.json()['data']['jobStats']['total']




    count = 0
    pickle_path = os.path.join('/Users/priyeshpatel/PycharmProjects/Nio_jobscrape/',datestr + name +'pickle_files_ponyai/')
    if not os.path.exists(pickle_path):
        os.makedirs(pickle_path)

    jobs = unpickling(pickle_path + 'jobs')
    if not jobs['data']:
        jobs = []
    else:
        jobs = jobs['data']
        # print(jobs)

    try:
        picklecount1 = unpickling(pickle_path + 'count')['data'] #.pop()
        print(picklecount1)
    except TypeError:
        picklecount1 = 0
    if not picklecount1:
        picklecount1 = 0


    for s in range(0, len(r)):

        for j in r[s]:
            count += 1
            if int(picklecount1) <= count:

                job_dict = {}
                job_company = ''
                job_title = ''
                job_department = ''
                working_years = ''
                job_salary = ''
                education = ''
                job_location = ''
                job_description = ''
                # print(count, j['id'], j['title'])

                cookies = {
                    'connect.sid': 's%3AILYGonTpvjlhv49LmgF3QBb0CqEy5Dn4.supUvSifjujOJ5eNmpmhDoXPThChu1mo%2FAO4orPz8Gc',
                    'locale': 'zh-CN',
                    'acw_tc': '2760826116290074194764281e5263e470256163bf2d5c9ea42ee5ac733acb',
                    'moka-apply': 'HLZ0g7oEc%2FNb4TxZ5bA72%2BfJqKewALyyjerW8GhTBEw%3D',
                }

                headers = {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                    'Accept-Language': 'en-gb',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Host': 'app.mokahr.com',
                    'Origin': 'https://app.mokahr.com',
                    'Content-Length': '77',
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
                    'Referer': 'https://app.mokahr.com/apply/pony/2736',
                    'Connection': 'keep-alive',
                    'use-http-status': '0',
                    'moka-tracing': '{"op_no":"bfcc67ae-82ff-428f-8f09-b22e56f7ce7f","locale":"zh_CN","time_zone":"GMT+08:00","source":"apply-web","org_id":"pony"}',
                }

                data = '{"orgId":"pony", "jobId":"' + j['id'] + c


                response = requests.post('https://app.mokahr.com/api/outer/ats-jc-apply/website/job', headers=headers,
                                         cookies=cookies, data=data)
                try:
                    job_published = response.json()['data']["publishedAt"]
                except KeyError:
                    job_published = ''
                try:
                    job_description = BeautifulSoup(response.json()['data']["jobDescription"], 'lxml').text
                except KeyError:
                    job_description = ''
                try:
                    try:
                        city = cities[str(response.json()['data']["locations"][0]["cityId"])]
                    except KeyError:
                        city = str(response.json()['data']["locations"][0]["cityId"])
                    except ValueError:
                        city = str(response.json()['data']["locations"][0]["cityId"])
                    job_location = response.json()['data']["locations"][0]["address"] + '，cityId: ' + city
                except KeyError:
                    job_location = ''
                except IndexError:
                    job_location = ''
                try:
                    job_company = '小马智行'
                except KeyError:
                    job_company = '小马智行'
                job_title = response.json()['data']["title"]
                try:
                    job_department = 'department: ' + response.json()['data']["department"]["name"] + ', zhineng: ' + response.json()['data']["zhineng"]["name"]
                except KeyError:
                    job_department = ''
                job_salary = ''
                job_education = ''
                working_years = ''
                job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department, ' 工作年限': working_years,
                            '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD（经验，工作内容）': job_description, 'Published': job_published}

                print(picklecount1, count, job_company, job_department, job_title, job_salary, job_location)
                jobs.append(job_dict)
                pickling(jobs, pickle_path + 'jobs')
                pickling(count, pickle_path + 'count')

    data=pd.DataFrame(jobs)
    data.to_excel(rootPath + datestr + '_' + timestr + '_' + job_company + name +'.xlsx', engine='xlsxwriter')



