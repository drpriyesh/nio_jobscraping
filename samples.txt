XIAOMI
{code: 0,…}
code: 0
data: {aiCurrentAccuracy: 0.5, aiThresholdAccuracy: 0.5, attributeId: 791, autoArchiveTalentPoolId: 2882,…}
aiCurrentAccuracy: 0.5
aiThresholdAccuracy: 0.5
attributeId: 791
autoArchiveTalentPoolId: 2882
autoDistribution: 0
autoFilterRuleId: 34913
autoFilterThreshold: 0.3
campusSites: []
category2Id: "1.1.1"
createMethod: "手动添加"
createdAt: "2021-08-13T09:34:54"
creatorId: 966249
department: {id: 32937, name: "小米公司"}
hireMode: 1
id: "1053c986-8e74-48d9-8e14-2aaecef08bde"
isConditionalAutoFilter: 1
isShowRecommendSite: true
jobDescription: "<p>负责小米智能音箱、智能摄像头产品中视觉相关工作，包括linux系统下视频的流媒体编码上传及下载播放工作，视觉AI算法的集成或开发等。</p><p> 要求：</p><ol><li>熟悉linux或RTOS，熟悉C/C++语音</li><li>熟悉主流的音视频编解码协议（H264/H265/MPEG4/OPUS等）</li><li>熟悉主流的流媒体协议者优先（RTSP、RTMP、RTP等）</li><li>熟悉FFmpeg，有深度优化经验者优先</li><li>对接过图像AI技术，懂得图像处理算法原理者优先</li></ol><p><br></p>"
jobNumId: 2930001
jobRankIds: [207]
jobTemplateId: 0
locations: [{address: "小米科技园G栋", cityId: 110108, country: "中国", id: 50532}]
mjCode: "MJ030297"
openedAt: "2021-08-13T00:00"
orgId: "xiaomi"
pipelineId: 550
postRequestResult: 0
prior: 1
publishedAt: "2021-08-16T12:29:35"
recommendAccessTimes: 0
recommendShareTimes: 0
recommendationBonus: 0
recommendationReason: {notNull: true, require: true}
shmInterview: 0
shmNextInterview: 0
showCampusSites: false
showIsCampus: false
status: "open"
title: "小米生态链-视频软件工程师"
updatedAt: "2021-08-16T12:29:34"
useHeadhunter: 0
virtual: 0
zhineng: {id: 5286, name: "软件研发类"}
msg: "成功"
success: true


HUAWEI
{deptNickName: null, deptDescription: null, jobNameOld: null,…}
appDate: null
appFlag: null
appParentStatus: null
appParentStatusDesc: null
appStatus: null
appStatusDesc: null
areaCode: null
areaCodes: null
areaName: null
cancelflag: null
cityCode: null
cityCodes: null
cityId: null
cityIdArray: null
cityIdStr: null
cityIds: null
cityName: null
countryCode: null
countryCodeRegistry: null
countryCodes: null
countryIdArray: null
countryIdStr: null
countryName: "China"
creationDate: "2021-08-16T00:00:00.000+0800"
creationUserCN: null
degreeCode: null
degreeCodes: null
deployDate: null
deptCode: null
deptCodes: null
deptDescription: null
deptName: "全球技术服务部"
deptNickName: null
destineUserId: null
favFlag: null
filterDate: null
headSpell: null
intentName: null
interviewDate: null
intervieweeId: null
intervieweeStatus: null
intervieweeStatusValue: null
isHotJob: null
isSCResume: null
isSubmitQuest: null
issDays: null
issueDays: null
jobAddress: "600216,600338,600321,600428,"
jobArea: "中国/东莞;中国/南京;中国/长沙;中国/济南"
jobBoxroomId: null
jobChannelId: 1
jobColUid: null
jobDesc: null
jobFamClsCode: null
jobFamilyCode: null
jobFamilyCodes: null
jobFamilyName: "技术族"
jobId: 186231
jobIntent: null
jobList: null
jobLists: null
jobMailingId: null
jobNameOld: null
req=jobRequire: "业务技能要求：\n1.交互设计能力，视觉设计能力；\n2.TOB/TOC 行业经验；\n3.团队合作和交流能力。\n专业知识要求：\n1.5年以上TO B或者TO C云服务相关业务经验及行业经验；\n2.具有3年以上交互相关经验；\n3.具备面向2B行业客户多年交互设计经验，或参与过业界头部云服务产品交互设计经验优先。"
jobRequirementId: 195087
jobType: "1"
jobTypeList: null
jobTypes: null
jobname: "交互设计专家"
keywords: null
language: "zh_CN"
languageCode: null
languageName: null
languages: null
lastUpdateDate: null
lastUpdateUserCN: null
mailingNum: null
desc=mainBusiness: "1.负责推动对于软件的产品、运营、市场所涉及到的交互产品设计的创新设计；\n2.优化产品交互和设计规范，保证产品交互和视觉交付的质量，提升产品体验的一致性、美感和调性达到业界先进水平；\n3.建立产品用户体验的保障机制，倾听用户声音，持续优化产品用户体验，通过优秀交互设计促进用户数量提升。"
offerDate: null
offerId: null
offerState: null
onboardingDate: null
orderBy: null
pri: null
refuseReason: null
region: null
resumeId: null
resumeIds: null
rowIdx: -1
searchType: null
size: null
spell: null
spellType: null
status: null
type: null
vacancynum: 1
workYearCode: null
workYearCodeL: null
workYearCodes: null



LIXIANG
{code: 0, msg: "SUCCESS",…}
code: 0
data: {id: 5026, title: "冲压设备维修工程师", number: 3, openedTime: "2021-08-13T00:00:00+08:00", minSalary: 0,…}
commitment: "全职"
departmentName: "设备管理室"
description: "<p>岗位职责：</p><p>1.\t编制冲压设备维护保养OIS/WES, 监控OIS/WES的实施。</p><p>2.\t对设备运转过程中存在的问题准确判断并及时解决问题。</p><p>3.\t支持生产团队的工作，及时处理生产过程中发生的问题，保证生产线的正常运转</p><p>4.\t及时完成设备的点检、保养等预防性维护工作，监控设备的运行状况；</p><p>5.\t对设备运转过程中发生的问题、产生原因及解决方法进行分类汇总，进行技术积累；</p><p>6.\t配合设备主管工程师及时完成设备维修档案的记录管理，对设备的运转情况熟悉掌握；</p><p>任职要求：</p><p>1.\t本科以上学历，机械电气及自动化  </p><p>1.\t从事冲压机械压力机和液压机维修等工作6年以上工作； </p><p>2.\t掌握机械压力机和液压压力机的原理，能够看懂液压油路原理图、机械压力机结构图、电气原理图，根据原理图，能够对设备运转过程中存在的问题准确判断并及时解决问题； </p><p>3.\t能够独立完成设备的点检、保养、设备管理文件的编制及实施等工作；</p><p>4.\t具备对设备运转过程中发生的问题进行分析和解决问题的能力；</p><p>5.\t有吃苦耐劳精神和团队协作精神；</p>"
education: ""
hireMode: "社招"
id: 5026
isPrior: 1
locationAddress: "江苏省常州市武进区凤林南路108号"
locationCity: "江苏常州市武进区"
maxExperience: 5
maxSalary: 0
minExperience: 3
minSalary: 0
number: 3
openedTime: "2021-08-13T00:00:00+08:00"
title: "冲压设备维修工程师"
zhinengName: "生产制造"
msg: "SUCCESS"
success: true


SENSETIME
{state: "200", type: "success",…}
data: {applyNumByProject: 3, bindAccessPaper: false, bindWrittenPaper: false, canDelivery: true,…}
applyNumByProject: 3
bindAccessPaper: false
bindWrittenPaper: false
canDelivery: true
canUserActyco: false
company: "商汤科技"
department: "AI游戏事业部"
distance: 0
education: "本科"
gender: "不限"
isCollection: 0
isHaveSite: 0
isHaveVolunteer: 0
orgCode: "0/7"
orgConfig: "{\"orgIntroduction\":\"\",\"orgLogo\":{\"imgUrl\":\"http://wecruit.oss-cn-hangzhou.aliyuncs.com/files/default/224c1ea8-7dc3-47a0-8114-61bc95cf4cd6.png\",\"imgId\":{\"blob\":{},\"name\":\"logo_首页.png\"}},\"orgImg\":{\"imgUrl\":\"http://wecruit.oss-cn-hangzhou.aliyuncs.com/files/default/60efb869-f060-48b7-9bfe-5c8a904b0acb.png\",\"imgId\":{\"blob\":{},\"name\":\"logo_企业微信logo.png\"}}}"
orgLogoUrl: "http://wecruit.oss-cn-hangzhou.aliyuncs.com/files/default/224c1ea8-7dc3-47a0-8114-61bc95cf4cd6.png"
orgName: "AI游戏事业部"
pageViews: 2
postId: "6119fe68bef57c65330ff204"
postName: "游戏AI交付开发"
postType: "0/1227/37850543"
postTypeName: "技术族"
projectId: 0
projectName: "社会招聘"
publishDate: "2021-08-16"
quickResumeTemplateId: "604c6a7d0dcad42ae4631254"
recruitNum: 1
recruitNumStr: "1"
recruitType: 2
resumeTemplateId: "604c6a7d0dcad42ae4631251"
serviceCondition: "1、热爱游戏行业，对游戏+AI领域有兴趣\r\n2、有游戏开发经验，熟悉游戏引擎的使用（有SLG游戏开发经验最佳）\r\n3、有SLG或其他品类游戏行为树开发经验最佳\r\n4、coding与工程构架能力强，熟悉python等主流语言，熟悉Linux开发环境，对计算机系统和网络原理有一定了解\r\n5、有技术信仰，对新技术和不确定性充满野心和激情"
showDeliverButton: 1
top: 0
totalApplyNum: 25
workContent: "主要参与游戏AI项目研发并解决AI交付中的相关问题。\r\n1、复杂游戏场景的底层环境和配合完成AI环境搭建（python为主）\r\n2、AI算法的开发及调优（技术栈：数值优化、行为树等）\r\n备注：阶段性主要解决行为树及游戏AI客户端交付对接过程中的工程实现"
workPlaceStr: "上海市"
workTypeStr: "全职"
state: "200"
type: "success"

PONYAI
{code: 0,…}
code: 0
data: {aiCurrentAccuracy: 0, aiThresholdAccuracy: 0, attributeId: 858, autoArchiveTalentPoolId: 7828,…}
aiCurrentAccuracy: 0
aiThresholdAccuracy: 0
attributeId: 858
autoArchiveTalentPoolId: 7828
autoDistribution: 0
autoFilterThreshold: 0
campusSites: []
createMethod: "手动添加"
createdAt: "2021-08-16T11:46:29"
creatorId: 1499900
department: {id: 14792, name: "软件研发"}
hireMode: 1
id: "1846e0a3-97c1-4473-a2b8-fad673b510d5"
isConditionalAutoFilter: 1
isShowRecommendSite: true
jobDescription: "<p>岗位职责：</p><ul><li>设计自动驾驶硬件系统中传感器数据融合和处理的相关硬件；</li><li>设计自动驾驶车载计算平台相关电子电路系统；</li><li>参与自动驾驶整车软硬件联合调试与系统集成；</li><li>参与自动驾驶计算平台与传感器设备的选型评估。</li></ul><p>任职要求：</p><ul><li>良好的模拟电路，数字电路基础，对线性电源，开关电源有深入的理解</li><li>熟悉嵌入式硬件设计、调试经验，掌握常用总线接口协议及测试方法；</li><li>熟悉EMC试验标准以及并能够对EMC试验问题做对策整改；</li><li>熟练掌握主流原理图及PCB设计工具（Cadence,Pads等），能够独立完成电路设计及投产工作；</li><li>有以太网相关的硬件设计经验，熟悉以太网工作原理及具有相关测试经验</li><li>有车载电子系统设计及量产经验，有ADAS产品开发、设计、测试、量产经验者加分</li><li>熟悉汽车电子产品的开发测试流程，熟悉ISO26262，TS16949等汽车行业相关标准者加分；</li><li>熟悉Linux系统，有Linux下的嵌入式开发调试经验者加分；</li></ul>"
jobNumId: 2938804
jobRankIds: [611]
jobTemplateId: 0
locations: [{address: "北清路81号 中关村壹号 A1座19\20层", cityId: 110108, country: "中国", id: 50400}]
mjCode: "MJ000624"
openedAt: "2021-08-16T00:00"
orgId: "pony"
pipelineId: 1140
postRequestResult: 0
prior: 1
publishedAt: "2021-08-16T11:46:28"
recommendAccessTimes: 0
recommendShareTimes: 0
recommendationBonus: 0
recommendationReason: {notNull: true, require: true}
shmInterview: 0
shmNextInterview: 0
showCampusSites: false
showIsCampus: false
status: "open"
title: "硬件系统工程师EE-Truck"
updatedAt: "2021-08-16T13:49:53"
useHeadhunter: 0
virtual: 0
zhineng: {id: 15036, name: "硬件研发"}
msg: "成功"
success: true