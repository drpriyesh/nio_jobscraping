import requests
import pandas as pd
import time
import set_path
rootPath = set_path.root_path()
datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")

cookies = {
    'SERVERID': 'b8bba6aba8d2e44d0648d2a625e2ddb4|1629004474|1629003779',
    '8539273bfa333d53_gr_session_id': 'f306279d-4e27-46ed-ba6d-46f999673095',
    '8539273bfa333d53_gr_session_id_f306279d-4e27-46ed-ba6d-46f999673095': 'true',
    'Hm_lpvt_8ebb5ff06a2c44c46939dec8d61388ed': '1629003773',
    'Hm_lvt_8ebb5ff06a2c44c46939dec8d61388ed': '1629003072',
    '_ga': 'GA1.2.1722382290.1629003070',
    '_gid': 'GA1.2.244649809.1629003070',
    'gr_user_id': '9c0e137f-7f79-49db-99e1-1b01304a4014',
    '_gcl_au': '1.1.2004654354.1629003070',
}

headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Origin': 'https://hr.sensetime.com',
    'Content-Length': '53',
    'Accept-Language': 'en-gb',
    'Host': 'hr.sensetime.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'https://hr.sensetime.com/SU604c56f9bef57c3d1a752c60/pb/social.html',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
}

params = (
    ('iSaJAx', 'isAjax'),
    ('t', '1629004474596'),
)

data = {
  'isFrompb': 'true',
  'recruitType': '2',
  'pageSize': '15',
  'currentPage': '1'
}

response = requests.post('https://hr.sensetime.com/wecruit/positionInfo/listPosition/SU604c56f9bef57c3d1a752c60', headers=headers, params=params, cookies=cookies, data=data)


jobsperpage = response.json()['data']['pageForm']["pageSize"]
totalpages = response.json()['data']['pageForm']["totalPage"]
totaljobs = response.json()['data']['pageForm']['dataCount']
job_ids = []
count = 0
for i in range(1, int(totalpages)+1):
    data = {
        'isFrompb': 'true',
        'recruitType': '2',
        'pageSize': '15',
        'currentPage': str(i)
    }
    response = requests.post('https://hr.sensetime.com/wecruit/positionInfo/listPosition/SU604c56f9bef57c3d1a752c60', headers=headers, params=params, cookies=cookies, data=data)


    for r in response.json()['data']['pageForm']['pageData']:
        count+=1
        print(count, r['postId'], r['postName'])
        job_ids.append(r['postId'])

jobs = []
count = 0
cookies = {
    'SERVERID': 'b8bba6aba8d2e44d0648d2a625e2ddb4|1629003984|1629003779',
    '8539273bfa333d53_gr_session_id': 'f306279d-4e27-46ed-ba6d-46f999673095',
    '8539273bfa333d53_gr_session_id_f306279d-4e27-46ed-ba6d-46f999673095': 'true',
    'Hm_lpvt_8ebb5ff06a2c44c46939dec8d61388ed': '1629003773',
    'Hm_lvt_8ebb5ff06a2c44c46939dec8d61388ed': '1629003072',
    '_ga': 'GA1.2.1722382290.1629003070',
    '_gid': 'GA1.2.244649809.1629003070',
    'gr_user_id': '9c0e137f-7f79-49db-99e1-1b01304a4014',
    '_gcl_au': '1.1.2004654354.1629003070',
}

headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Origin': 'https://hr.sensetime.com',
    'Content-Length': '31',
    'Accept-Language': 'en-gb',
    'Host': 'hr.sensetime.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'https://hr.sensetime.com/SU604c56f9bef57c3d1a752c60/pb/posDetail.html?postId=61167ef20dcad4106f001a9c&postType=society',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
}

params = (
    ('iSaJAx', 'isAjax'),
    ('t', '1629003984434'),
)
jobs = []
count = 0
for job_id in job_ids:
    job_dict = {}
    job_company = ''
    job_title = ''
    job_department = ''
    working_years = ''
    job_salary = ''
    education = ''
    job_location = ''
    job_description = ''
    data = {
      'postId': str(job_id)
    }
    count += 1
    response = requests.post('https://hr.sensetime.com/wecruit/positionInfo/listPositionDetail/SU604c56f9bef57c3d1a752c60', headers=headers, params=params, cookies=cookies, data=data)

    r = response.json()['data']
    job_company = r['company']
    job_title = r['postName']
    try:
        postTypeName = 'postTypeName: ' + r['postTypeName']
    except KeyError:
        postTypeName = ''
    try:
        projectName = 'projectName: ' + r['projectName']
    except KeyError:
        projectName = ''
    try:
        workTypeStr = 'workTypeStr: ' + r['workTypeStr']
    except KeyError:
        workTypeStr = ''
    other = postTypeName + ', ' + projectName + ', ' + workTypeStr
    try:
        job_department = r['department']
    except KeyError:
        job_department = ''
    working_years = ''
    job_salary = ''
    try:
        job_education = r['education']
    except KeyError:
        job_education = ''
    job_location = r['workPlaceStr']
    job_requirements = r['serviceCondition']
    job_description = r['workContent']

    try:
        job_published = r["publishDate"]
    except KeyError:
        job_published = ''

    job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department, ' 工作年限': working_years,
                '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD (requirements)': job_requirements,'JD（经验，工作内容）': job_description,
                'Published': job_published, 'Other': other}

    print(count, job_company, job_department, job_title, job_salary, job_location)
    jobs.append(job_dict)

data=pd.DataFrame(jobs)
data.to_excel(rootPath + datestr + '_' + timestr + '_' + job_company +'.xlsx', engine='xlsxwriter')