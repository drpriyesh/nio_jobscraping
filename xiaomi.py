import requests
import pandas as pd
import os
from pickle_dat import pickling, unpickling
import xlsxwriter
import time
from bs4 import BeautifulSoup
import set_path
rootPath = set_path.root_path()
datestr = time.strftime("%Y%m%d")
timestr = time.strftime("%H%M%S")

cookies1 = {
    'connect.sid': 's%3A4l4BH4q8w6JOWJaUiynwJ5pjez-NM_cw.6wklZkauUx2ZKqS3BCJyOaKa3qoSi0tjOQQRUkNl1XQ',
    'locale': 'zh-CN',
    'pageid': '67e6174edc37d132',
    'msttime': 'https%3A%2F%2Fhr.xiaomi.com%2F',
    'mstuid': '1628906970150_8221',
    'mstz': '||1985231113.7||https%253A%252F%252Fwww.mi.com|https%25253A%25252F%25252Fwww.mi.com',
    'xm_vistor': '1628906970150_8221_1629290536919-1629291057717',
    'hd_log_code': 'undefined',
    'log_code': '67e6174edc37d132-23386886cf5a7b81|https%3A%2F%2Fhr.xiaomi.com%2F',
    'moka-apply': 'Gfu791aRt32sNGEl%2BZHB0rMIK4bmcxG7axb9qKqVAbk%3D',
}

headers1 = {
    'Content-Type': 'application/json',
    'Accept': '*/*',
    'Accept-Language': 'en-gb',
    'Accept-Encoding': 'gzip, deflate',
    'Host': 'job.hr.xiaomi.com',
    'Origin': 'http://job.hr.xiaomi.com',
    'Content-Length': '85',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'http://job.hr.xiaomi.com/',
    'Connection': 'keep-alive',
    'use-http-status': '0',
    'moka-tracing': '{"op_no":"5f66c457-698f-4685-b15f-d3819f917076","locale":"zh_CN","time_zone":"GMT+08:00","source":"apply-web","org_id":"xiaomi"}',
}

cookies2 = {
    'connect.sid': 's%3A4l4BH4q8w6JOWJaUiynwJ5pjez-NM_cw.6wklZkauUx2ZKqS3BCJyOaKa3qoSi0tjOQQRUkNl1XQ',
    'locale': 'zh-CN',
    'pageid': '67e6174edc37d132',
    'mstuid': '1628906970150_8221',
    'mstz': '||1985231113.7||https%253A%252F%252Fwww.mi.com|https%25253A%25252F%25252Fwww.mi.com',
    'xm_vistor': '1628906970150_8221_1629290536919-1629291057717',
    'hd_log_code': 'undefined',
    'log_code': '67e6174edc37d132-23386886cf5a7b81|https%3A%2F%2Fhr.xiaomi.com%2F',
    'moka-apply': 'Gfu791aRt32sNGEl%2BZHB0rMIK4bmcxG7axb9qKqVAbk%3D',
}

headers2 = {
    'Content-Type': 'application/json',
    'Accept': '*/*',
    'Accept-Language': 'en-gb',
    'Accept-Encoding': 'gzip, deflate',
    'Host': 'job.hr.xiaomi.com',
    'Origin': 'http://job.hr.xiaomi.com',
    'Content-Length': '78',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15',
    'Referer': 'http://job.hr.xiaomi.com/',
    'Connection': 'keep-alive',
    'use-http-status': '0',
    'moka-tracing': '{"op_no":"5f66c457-698f-4685-b15f-d3819f917076","locale":"zh_CN","time_zone":"GMT+08:00","source":"apply-web","org_id":"xiaomi"}',
}



data = '{"limit":15,"offset":0,"siteId":287,"orgId":"xiaomi","site":"social","needStat":true}'
limit=15
response = requests.post('http://job.hr.xiaomi.com/api/outer/ats-jc-apply/website/jobs', headers=headers1, cookies=cookies1, data=data)
no_jobs = int(response.json()['data']['jobStats']['total'])
no_pages = int(no_jobs/limit)+1
r=[]
count = 0
for i in range (0,no_jobs,limit):
    # print(i)
    data = '{"limit":' + str(limit) + ',"offset":' + str(i) + ' ,"siteId":287,"orgId":"xiaomi","site":"social","needStat":true}'
    response = requests.post('http://job.hr.xiaomi.com/api/outer/ats-jc-apply/website/jobs', headers=headers1, cookies=cookies1, data=data)
    for line in response.json()['data']['jobs']:
        count += 1
        print(count, line['title'])

    r.append(response.json()['data']['jobs'])
    no_jobs = response.json()['data']['jobStats']['total']




count = 0
pickle_path = os.path.join('/Users/priyeshpatel/PycharmProjects/Nio_jobscrape/',datestr + 'pickle_files_xiaomi/')
if not os.path.exists(pickle_path):
    os.makedirs(pickle_path)

jobs = unpickling(pickle_path + 'jobs')
if not jobs['data']:
    jobs = []
else:
    jobs = jobs['data']
    # print(jobs)

try:
    picklecount1 = unpickling(pickle_path + 'count')['data'] #.pop()
    print(picklecount1)
except TypeError:
    picklecount1 = 0
if not picklecount1:
    picklecount1 = 0


for s in range(0, len(r)):

    for j in r[s]:
        count += 1
        if int(picklecount1) <= count:

            job_dict = {}
            job_company = ''
            job_title = ''
            job_department = ''
            working_years = ''
            job_salary = ''
            education = ''
            job_location = ''
            job_description = ''

            # print(count, j['id'], j['title'])

            cookies = cookies2

            headers = headers2

            data = '{"orgId":"xiaomi", "jobId":"' + j['id'] + '", "siteId":287}'

            response = requests.post('http://job.hr.xiaomi.com/api/outer/ats-jc-apply/website/job', headers=headers2,
                                     cookies=cookies2, data=data)


            try:
                job_published = response.json()['data']["publishedAt"]
            except KeyError:
                job_published = ''
            try:
                job_description = BeautifulSoup(response.json()['data']["jobDescription"]).text
            except KeyError:
                job_description = ''
            if not response.json()['data']["locations"]:
                job_location = ''
            elif response.json()['data']["locations"]:
                try:
                    job_location = response.json()['data']["locations"][0]["address"]
                except KeyError:
                        job_location = ''
            try:
                job_company = response.json()['data']["department"]["name"]
            except KeyError:
                job_company = '小米公司'
            job_title = response.json()['data']["title"]
            try:
                job_department = response.json()['data']["zhineng"]["name"]
            except KeyError:
                job_department = ''
            job_salary = ''
            job_education = ''
            working_years = ''
            job_dict = {'友商': job_company, '职位名': job_title, '部门': job_department,
                        ' 工作年限': working_years,
                        '薪资范围': job_salary, '学历': job_education, '地点': job_location, 'JD（经验，工作内容）': job_description,
                        'Published': job_published}

            print(picklecount1, count, job_company, job_department, job_title, job_salary, job_location)
            jobs.append(job_dict)

            pickling(jobs, pickle_path + 'jobs')
            pickling(count, pickle_path + 'count')

data=pd.DataFrame(jobs)
data.to_excel(rootPath + datestr + '_' + timestr + '_' + job_company +'.xlsx', engine='xlsxwriter')

# "jobDescription"
# "title"
# "useHeadhunter"
# "zhineng": {
#             "name": "其他"
#         }
# "locations": [
#             {
#                 "address": "安宁庄路小米科技园",
#                 "cityId": 110108,
#                 "country": "中国",
#                 "id": 47419
#             }
#         ],




